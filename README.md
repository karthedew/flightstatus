# flightstatus

This program was developed to create a doubly linked-list data structure and
work on simple sorting algorithms while practicing C/C++ and object oriented 
programming skills.

## Setup

If you wish to run thos program, you need to make sure you have g++ installed on
a windows environment. This program has not been tested or compiled for Linux
environments.

This program was developed inside of **Microsoft Visual Studio Code** with the
support of the following VS Code add-ons:

 - Cmake
 - item 2
 - item 3

When you compile this code, the output executable will be named "a.exe".


# Elements of the Code

## Doubly Linked-List

The doubly linked-list (DLL) was utilized for creating a data strcuture of the
list of airports within the United States. These are read in from the
airports.txt, UTF-8 unicode, text file.

Each node in the DLL is composed of the Airport class containing the name, city,
state, IATA, and ID associated with each individual airport. Both *next and
*prev pointers are used to linked each Airport object to each other within the
DLL. The Airport ID is determined by converting each character of the IATA
string to its proper decimal and then summing these integers together. Because
each IATA will be unique, there should be no duplicate integer values. This is
important for sorting these 

The Airports class contains *head and *tail pointers for the beginning and end
of the DLL. This class also contains the algorithm to sort, add, and delete,
along with other operations that would be performed on the DLL.
